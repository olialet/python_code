"""
The aim of the script is applying Distagger in order to obtain information about disfluences
and its summarized and detailed forms used for update of initial xml-formatted files
Input params:
@distaggerpath - path to Distagger tool, @inputdir - path to dir with normalized files, @xmlfilespath - path with xml files,
@keeptmpfiles -  put 1 if you want to keep snt files produced by Distagger
(it will be kept in tmpresdistag folder of the current directory)
"""
distaggerpath = "C:/Python34/myproject/distagger-0.2/"
inputdir = "C:/Python34/myproject/normalise/" 
xmlfilespath = "C:/Python34/myproject/XMLfile/"


import xml.etree.ElementTree as etree
from itertools import groupby
#from utilsSLAM import *
import shutil
import os #remove after
import re #remove after
from collections import Counter

def listDirectory(directory, fileExtList):                                        
    "get list of file info objects for files of particular extensions" 
    fileList = [os.path.normcase(f)
                for f in os.listdir(directory)]             
    Filelist = [os.path.join(directory, f) 
               for f in fileList
                if os.path.splitext(f)[1] in fileExtList] 
    for i in fileList:
        if re.match(r'.*\.D.*', i):
            fileList.remove(i)
    return fileList
#produire les analyses des disfluences grace a Distagger. Arguments : le chemin vers le repertoire qui continet distagger, le fichier sur lequel on souhaite avoir les annotations en disfluence, le répertoire dans lequel on stocke les resultats.
def Distagger(cheminDistagger, fichier, dest):
    os.system('java -jar '+cheminDistagger+'build/jar/Distagger-0.2.jar -e '+cheminDistagger+'data/euh.txt -o . -i  '+cheminDistagger+'data/insertions.txt -c  '+cheminDistagger+'data/autocorrections.txt -m '+cheminDistagger+'data/meta.txt -t '+fichier)
    os.system('mv *.snt '+dest)
    os.system('mv *.rha '+dest)
"""
Method prefiltre used for eliminating IGN+REP disfluency tags
for the list of words(oui, non etc.), because of its insignificance for the result.
"""
def prefiltre(lines):
    tmp = []
    for res in lines:
        res = re.sub(r" \/","",res)#todo to check if resolves problem with lengths
        res = re.sub(r'[Vv]oilà', 'voila',res)
        res = re.sub(r'\{oui,\.IGN\+REP\}', 'oui', res)
        res = re.sub(r'\{oui oui,\.IGN\+REP\}', 'oui oui', res)
        res = re.sub(r'\{(ouais\s*?)*,\.IGN\+REP\}', 'ouais', res)
        res = re.sub(r'\{ouai,\.IGN\+REP\}', 'ouai', res)
        res = re.sub(r'\{non,\.IGN\+REP\}', 'non', res)
        res = re.sub(r'\{ok,\.IGN\+REP\}', 'ok', res)
        res = re.sub(r'\{hum,\.IGN\+REP\}', 'hum', res)
        res = re.sub(r'\{hum hum,\.IGN\+REP\}', 'hum hum', res)
        res = re.sub(r"\{mmh,\.IGN\+REP\}", 'mmh', res)
        res = re.sub(r"\{mmh mmh,\.IGN\+REP\}", 'mmh mmh', res)
        res = re.sub(r'\{vous,\.IGN\+REP\}', 'vous', res)
        res = re.sub(r'\{hm,\.IGN\+REP\}', 'hm', res)
        res = re.sub(r"\{(c' est sûr\s*?)*?,\.IGN\+REP\}", 'c\' est sûr', res)
        res = re.sub(r"\{(voilà\s*?)*?\/,\.IGN\+REP\}","voilà",res)
        res = re.sub(r"\{(euh)\s(voilà\s*?)*?\/,\.IGN\+REP\}","euh voilà",res)
        res = re.sub(r"\{(si\s*?)*?,\.IGN\+REP\}", 'si', res)
        res = re.sub(r"\{(nan\s*?)*?,\.IGN\+REP\}", 'nan', res)
        res = re.sub(r"\{(d'\s*?accord\s*?)*?,\.IGN\+REP\}", "d' accord", res)
        res = re.sub(r"\{(bien sûr\s*?)*?,\.IGN\+REP\}", 'bien sûr', res)
        res = re.sub(r"\{(etcétéra),\.IGN\+REP\}", 'etcétéra', res)
        tmp.append(res)
    return tmp

"""
Method performdetailedupdate designed to update xml file with disfluency tags,
matching exact words or positions in the speech(for pause)

inputs:
    @xmlfile is the path to the xml file
    @resdistagfile is the path for snt file with Distagger results for @xmlfile
output:
     no output is produced
     @xmlfile is updated with the data from @resdistagfile
"""
def performdetailedupdate(xmlfile,resdistagfile):
    with open(resdistagfile,"r",encoding='utf-8') as f:

        filelines = f.readlines()
        filelines = prefiltre(filelines)

        tree = etree.ElementTree(file=xmlfile)
        root = tree.getroot()
        utterences = root.findall("./topic/")

        filelines.remove(filelines[0])
        filelines.remove(filelines[len(filelines)-1])

        filelines = list(map(lambda i: re.sub(r'(\{.*IGN\+speaker?\})',"",i),filelines))
        filelines = list(map(lambda i: re.sub(r'(\{S\})',"",i),filelines))
        j = 0 #iterator for utterances and filelines

        for line in filelines:
            wordnodes = utterences[j].findall("./word/")
            tmp = line.split() #tmp list of all words in a line
            tmp = [i for i in tmp if  i != "-"]
            tmp = [i for i in tmp if  i != "/"]
            nodes = []
            k = 0
            while k < len(tmp):
                word = tmp[k]
                if "{" not in word and "}" not in word:
                    nodes.append(wordnodes[k])

                if re.search(r"\{\/\,\.IGN\+short_pause\}",word):
                    tmpnode = etree.Element("IGN_short_pause")
                    nodes.append(tmpnode)
                    tmp.remove(word)
                    k = k - 1
                else:

                    if "{" in word and "}" in word:
                        disf = re.search(r"(IGN\+.*)",word).group()# extract tag
                        disf = re.sub(r"\+","_",disf)[:-1]
                        tmpnode = etree.Element(disf)
                        tmpnode.insert(0, wordnodes[k])
                        nodes.append(tmpnode)

                    else:
                        if "{" in word:
                            startindex = tmp.index(word)
                            indforend = []
                            for i in range(startindex,len(tmp)):
                                if "}" in tmp[i]:
                                    indforend.append(i)
                            endindex = indforend[0]
                            disf = re.search(r"(IGN\+.*)",tmp[endindex]).group()# extract tag
                            disf = re.sub(r"\+","_",disf)[:-1]
                            tmpnode = etree.Element(disf)
                            n = 0
                            for word in wordnodes[startindex:endindex+1]:
                                tmpnode.insert(n, word)
                                n = n + 1
                            k = k + n - 1
                            nodes.append(tmpnode)

                k = k + 1
            for item in utterences[j]:
                utterences[j].remove(item)
            disfstats = []# disfluency statistics per utterance
            for node in nodes:
                utterences[j].append(node)
                if node.tag != "word":
                    disfstats.append(node.tag)
            disfstats = Counter(disfstats)
            if len(disfstats) > 0:
                for disf in disfstats.keys():
                    utterences[j].attrib[disf] = str(disfstats[disf])

            j = j + 1

        tree.write(xmlfile,encoding='utf-8')

"""
Main method - applydistagger.
Input files are normalized for Distagger format to .txt files in tmp directory,
the Distagger is applied, the results from .snt files are used to update initial .xml files.
Some of the words were filtered as not important as repetition disfluencies(e.g. mmh, hm, etcetera)
"""

def applydistagger(keeptmpfiles):
    #from nor construct the txt file for distagger in corresponding form
    tmppath = os.getcwd()+"\\tmp\\" # directory for nor files changed for Distagger processing into txt files
    tmpresdistagpath = os.getcwd()+"\\tmpresdistag\\" # directory for output .snt files of Distagger

    for norfile in listDirectory(inputdir, ".nor"):
        with open(inputdir+norfile,"r",encoding='utf-8') as f:
            if 'tmp' not in os.listdir(os.getcwd()):
                os.mkdir(tmppath)
            l = len(inputdir)
            textlist = f.readlines();
            #todo delete empty lines
            textlist = textlist[2:-1]
            debut = "<deb id=\""+ tmppath +norfile[l:-4]+ ".txt\""+">\n"
            fin = "<fin id=\""+ tmppath +norfile[l:-4]+ ".txt\""+">" #todo need \n for LYON and no \n for larochelle
            textlist = [debut] + textlist + [fin]
            textinstring = ''.join(textlist)
            with open(tmppath+norfile[l:-4]+".txt", "w",encoding="utf-8") as f:
                f.write(textinstring)
            #sauver(textinstring, tmppath+norfile[l:-4]+".txt")

    #apply distagger for every txt file in tmp directory and sed result files to tmpresdistag directory
    for distagfile in listDirectory(tmppath, ".txt"):
        if 'tmpresdistag' not in os.listdir(os.getcwd()):
            os.mkdir(tmpresdistagpath)
        Distagger(distaggerpath,distagfile ,tmpresdistagpath)

    xmlfiles = []
    for file in listDirectory(xmlfilespath, ".xml"):
        xmlfiles.append(file)

    for resdistagfile in listDirectory(tmpresdistagpath, ".snt"):
        barename = resdistagfile[len(tmpresdistagpath):-9]
        matchingxmlfile = xmlfilespath+barename+".xml"

        #summarizing disfluency tagging results for every snt file
        with open(resdistagfile,"r",encoding='utf-8') as f:
            filelines = f.readlines()
            speaker1 = [] #all disfluences for speaker 1
            speaker2 = [] #all disfluences for speaker 2
            for line in filelines:
                pattern = re.compile(r"(IGN\+.*?)\}")
                res = re.findall(pattern, line)

                if(re.search(r"spk1",line) != None):
                    for item in res:
                        if(item not in ("IGN+slot", "IGN+speaker")): # filter slot and speaker tags
                            speaker1.append(item.replace("+","_"))
                if(re.search(r"spk2",line) != None):
                    for item in res:
                        if(item not in ("IGN+slot", "IGN+speaker")):
                            speaker2.append(item.replace("+","_"))
            #creation of dictionaries where key is disfluency and value - occurance number
            disf_spk1 = {}
            disf_spk2 = {}

            # data for speaker 1
            speaker1.sort()
            values_disf_sp1 = [len(list(group)) for key, group in groupby(speaker1)]
            keys_disf_sp1 = sorted(set(speaker1))

            for (key,value) in zip(keys_disf_sp1,values_disf_sp1):
                disf_spk1[key] = value

            # data for speaker 2
            speaker2.sort()
            values_disf_sp2 = [len(list(group)) for key, group in groupby(speaker2)]
            keys_disf_sp2 = sorted(set(speaker2))

            for (key,value) in zip(keys_disf_sp2,values_disf_sp2):
                disf_spk2[key] = value

        #adding summarized distagger data to xml
        tree = etree.ElementTree(file=matchingxmlfile)
        root = tree.getroot()

        #adding tags of disfluences to the first speaker
        spk1node = root.find("./participants/speaker/[@name='spk1']")
        if root.find("./participants/speaker/[@name='spk1']/disfluences") == None:
            spk1disfnode = etree.SubElement(spk1node, "disfluences")
        else:
            spk1disfnode = root.find("./participants/speaker/[@name='spk1']/disfluences")
            spk1disfnode.clear()

        for key in disf_spk1:
            tmp = etree.SubElement(spk1disfnode, "number")
            tmp.attrib["type"] = key
            tmp.text = str(disf_spk1[key])

        #adding tags of disfluences to the second speaker
        spk2node = root.find("./participants/speaker/[@name='spk2']")

        if root.find("./participants/speaker/[@name='spk2']/disfluences") == None:
            spk2disfnode = etree.SubElement(spk2node, "disfluences")
        else:
            spk2disfnode = root.find("./participants/speaker/[@name='spk2']/disfluences")
            spk2disfnode.clear()

        for key in disf_spk2:
            tmp = etree.SubElement(spk2disfnode, "number")
            tmp.attrib["type"] = key
            tmp.text = str(disf_spk2[key])

        xmlfile = xmlfilespath+barename+".xml"
        tree.write(xmlfile,encoding='utf-8')
        performdetailedupdate(xmlfile, resdistagfile)

    shutil.rmtree(tmppath)#removing directory with txt files fomatted for Distagger
    if(not keeptmpfiles):
        shutil.rmtree(tmpresdistagpath)#removing Distagger resulting directory

applydistagger(1)