# My project's README
This repo contains different random Python scripts
I wrote for my linguistic project during my first Master year in University of Lorraine (Nancy, France).
The aim of the project was to parse the dialogues between schizophrenic patients/controls and doctors 
to find how the speech of patients resembles from normal speech.
The scripts attached show the disfluency analysis and its graphical illustrration generation.
