xmlfilespath = "C:/Python34/myproject/xmlfiles/"
import xml.etree.ElementTree as etree
import os
import re

def listDirectory(directory, fileExtList):                                        
    "get list of file info objects for files of particular extensions" 
    fileList = [os.path.normcase(f)
                for f in os.listdir(directory)]             
    Filelist = [os.path.join(directory, f) 
               for f in fileList
                if os.path.splitext(f)[1] in fileExtList] 
    for i in fileList:
        if re.match(r'.*\.D.*', i):
            fileList.remove(i)
    return fileList

corr_sums = {"S":0,"T":0,"P":0} # IGN_CORR
rep_sums = {"S":0,"T":0,"P":0} # IGN_REP
euh_sums = {"S":0,"T":0,"P":0} # IGN_EUH
frag_sums = {"S":0,"T":0,"P":0} # IGN_FRAG
shortpause_sums = {"S":0,"T":0,"P":0} # IGN_short_pause
total_sums = {"S":0,"T":0,"P":0}

allwordsT = 0 # temoin=control group
allwordsS = 0 #psycho
allwordsP = 0 #psychologist

#female
allwordsT_female = 0 # temoin=control group female
allwordsS_female = 0 #psycho female
corr_sums_female = {"S":0,"T":0} # IGN_CORR
rep_sums_female = {"S":0,"T":0} # IGN_REP
euh_sums_female = {"S":0,"T":0} # IGN_EUH
frag_sums_female = {"S":0,"T":0} # IGN_FRAG
shortpause_sums_female = {"S":0,"T":0} # IGN_short_pause
total_sums_female = {"S":0,"T":0}
#male
allwordsT_male = 0 # temoin=control group male
allwordsS_male = 0 #psycho male
corr_sums_male = {"S":0,"T":0} # IGN_CORR
rep_sums_male = {"S":0,"T":0} # IGN_REP
euh_sums_male= {"S":0,"T":0} # IGN_EUH
frag_sums_male = {"S":0,"T":0} # IGN_FRAG
shortpause_sums_male = {"S":0,"T":0} # IGN_short_pause
total_sums_male = {"S":0,"T":0}
	
for file in listDirectory(xmlfilespath, ".xml"):
	tree = etree.ElementTree(file=xmlfilespath+file)
	root = tree.getroot()
	if root.get("speakerid") == "control group":
		temoin = 1
	else:
		temoin = 0
	
	if root.get("speakergender") == "Male":
		gender = 1
	if root.get("speakergender") == "Female":
		gender = 0
	
	#calculate number of words for P, T and S	
	spk1node = root.find("./participants/speaker/[@name='spk1']")
	allwordsP = allwordsP + int(spk1node.get("word_num"))	
	spk2node = root.find("./participants/speaker/[@name='spk2']")
	if temoin == 1:
		allwordsT = allwordsT + int(spk2node.get("word_num"))
		if gender == 1:
			allwordsT_male = allwordsT_male + int(spk2node.get("word_num"))
		else:
			allwordsT_female = allwordsT_female + int(spk2node.get("word_num"))
	if temoin == 0:
		allwordsS = allwordsS + int(spk2node.get("word_num"))	
		if gender == 1:
			allwordsS_male = allwordsS_male + int(spk2node.get("word_num"))
		else:
			allwordsS_female = allwordsS_female + int(spk2node.get("word_num"))
	#calculate all sums for psychologist
	disfnodeP = root.find("./participants/speaker/[@name='spk1']/disfluences")
	numbers = disfnodeP.findall("number")
	for number in numbers:
		if number.get("type") == "IGN_CORR":
			corr_sums["P"] = corr_sums["P"] + int(number.text)
			
		if number.get("type") == "IGN_REP":
			rep_sums["P"] = rep_sums["P"] + int(number.text)
			
		if number.get("type") =="IGN_EUH":
			euh_sums["P"] = euh_sums["P"] + int(number.text)
			
		if number.get("type") == "IGN_FRAG":
			frag_sums["P"] = frag_sums["P"] + int(number.text)
			
		if number.get("type") == "IGN_short_pause":
			shortpause_sums["P"] = shortpause_sums["P"] + int(number.text)
			
	#calculate sums for patient or control group
	disfnodeS = root.find("./participants/speaker/[@name='spk2']/disfluences")
	numbers = disfnodeP.findall("number")
	for number in numbers:
		if number.get("type") == "IGN_CORR":
			if temoin == 1:
				corr_sums["T"] = corr_sums["T"] + int(number.text)
				if gender == 1:
					corr_sums_male["T"] = corr_sums_male["T"] + int(number.text)
				else:
					corr_sums_female["T"] = corr_sums_female["T"] + int(number.text)
			if temoin == 0:
				corr_sums["S"] = corr_sums["S"] + int(number.text)
				if gender == 1:
					corr_sums_male["S"] = corr_sums_male["S"] + int(number.text)
				else:
					corr_sums_female["S"] = corr_sums_female["S"] + int(number.text)
		if number.get("type") == "IGN_REP":
			if temoin == 1:
				rep_sums["T"] = rep_sums["T"] + int(number.text)
				if gender == 1:
					rep_sums_male["T"] = rep_sums_male["T"] + int(number.text)
				else:
					rep_sums_female["T"] = rep_sums_female["T"] + int(number.text)
			if temoin == 0:
				rep_sums["S"] = rep_sums["S"] + int(number.text)
				if gender == 1:
					rep_sums_male["S"] = rep_sums_male["S"] + int(number.text)
				else:
					rep_sums_female["S"] = rep_sums_female["S"] + int(number.text)
		if number.get("type") =="IGN_EUH":
			if temoin == 1:
				euh_sums["T"] = euh_sums["T"] + int(number.text)
				if gender == 1:
					euh_sums_male["T"] = euh_sums_male["T"] + int(number.text)
				else:
					euh_sums_female["T"] = euh_sums_female["T"] + int(number.text)
			if temoin == 0:
				euh_sums["S"] = euh_sums["S"] + int(number.text)
				if gender == 1:
					euh_sums_male["S"] = euh_sums_male["S"] + int(number.text)
				else:
					euh_sums_female["S"] = euh_sums_female["S"] + int(number.text)
		if number.get("type") == "IGN_FRAG":
			if temoin == 1:
				frag_sums["T"] = frag_sums["T"] + int(number.text)
				if gender == 1:
					frag_sums_male["T"] = frag_sums_male["T"] + int(number.text)
				else:
					frag_sums_female["T"] = frag_sums_female["T"] + int(number.text)
			if temoin == 0:
				frag_sums["S"] = frag_sums["S"] + int(number.text)
				if gender == 1:
					frag_sums_male["S"] = frag_sums_male["S"] + int(number.text)
				else:
					frag_sums_female["S"] = frag_sums_female["S"] + int(number.text)
		if number.get("type") == "IGN_short_pause":
			if temoin == 1:
				shortpause_sums["T"] = shortpause_sums["T"] + int(number.text)
				if gender == 1:
					shortpause_sums_male["T"] = shortpause_sums_male["T"] + int(number.text)
				else:
					shortpause_sums_female["T"] = shortpause_sums_female["T"] + int(number.text)
			if temoin == 0:
				shortpause_sums["S"] = shortpause_sums["S"] + int(number.text)
				if gender == 1:
					shortpause_sums_male["S"] = shortpause_sums_male["S"] + int(number.text)
				else:
					shortpause_sums_female["S"] = shortpause_sums_female["S"] + int(number.text)
	
print("output: ",corr_sums,rep_sums,euh_sums,frag_sums,shortpause_sums)
print(allwordsT ,allwordsS,allwordsP)

total_sums["T"] = str((corr_sums["T"]+rep_sums["T"]+shortpause_sums["T"]+euh_sums["T"]+frag_sums["T"])/allwordsT)
total_sums["P"] = str((corr_sums["P"]+rep_sums["P"]+shortpause_sums["P"]+euh_sums["P"]+frag_sums["P"])/allwordsP)

if allwordsS > 0:

	total_sums["S"] = str((corr_sums["S"]+rep_sums["S"]+shortpause_sums["S"]+euh_sums["S"]+frag_sums["S"])/allwordsS)
	corr_sums["S"] = str(corr_sums["S"]/allwordsS)
	rep_sums["S"] = str(rep_sums["S"]/allwordsS)
	shortpause_sums["S"] = str(shortpause_sums["S"]/allwordsS)
	frag_sums["S"] = str(frag_sums["S"]/allwordsS)
	euh_sums["S"] = str(euh_sums["S"]/allwordsS)
else:
	corr_sums["S"] = "No data"
	rep_sums["S"] = "No data"
	shortpause_sums["S"] = "No data"
	frag_sums["S"] = "No data"
	euh_sums["S"] = "No data"
	total_sums["S"] = "No data"
	
corr_sums["T"] = str(corr_sums["T"]/allwordsT)
corr_sums["P"] = str(corr_sums["P"]/allwordsP)

rep_sums["T"] = str(rep_sums["T"]/allwordsT)
rep_sums["P"] = str(rep_sums["P"]/allwordsP)

euh_sums["T"] = str(euh_sums["T"]/allwordsT)
euh_sums["P"] = str(euh_sums["P"]/allwordsP)

frag_sums["T"] = str(frag_sums["T"]/allwordsT)
frag_sums["P"] = str(frag_sums["P"]/allwordsP)

shortpause_sums["T"] = str(shortpause_sums["T"]/allwordsT)
shortpause_sums["P"] = str(shortpause_sums["P"]/allwordsP)

#female
if allwordsS_female > 0:
	euh_femaleS_nbwords = str(euh_sums_female["S"]/allwordsS_female)
	rep_femaleS_nbwords = str(rep_sums_female["S"]/allwordsS_female)
	frag_femaleS_nbwords = str(frag_sums_female["S"]/allwordsS_female)
	shortpause_femaleS_nbwords = str(shortpause_sums_female["S"]/allwordsS_female)
	corr_femaleS_nbwords = str(corr_sums_female["S"]/allwordsS_female)
else:
	euh_femaleS_nbwords = "0"
	rep_femaleS_nbwords = "0"
	frag_femaleS_nbwords = "0"
	shortpause_femaleS_nbwords = "0"
	corr_femaleS_nbwords = "0"
if allwordsT_female > 0:
	euh_femaleT_nbwords = str(euh_sums_female["T"]/allwordsT_female)
	rep_femaleT_nbwords = str(rep_sums_female["T"]/allwordsT_female)
	frag_femaleT_nbwords = str(frag_sums_female["T"]/allwordsT_female)
	shortpause_femaleT_nbwords = str(shortpause_sums_female["T"]/allwordsT_female)
	corr_femaleT_nbwords = str(corr_sums_female["T"]/allwordsT_female)
else:
	euh_femaleT_nbwords = "0"
	rep_femaleT_nbwords = "0"
	frag_femaleT_nbwords = "0"
	shortpause_femaleT_nbwords = "0"
	corr_femaleT_nbwords = "0"
#male
if allwordsS_male > 0:
	euh_maleS_nbwords = str(euh_sums_male["S"]/allwordsS_male)
	rep_maleS_nbwords = str(rep_sums_male["S"]/allwordsS_male)
	frag_maleS_nbwords = str(frag_sums_male["S"]/allwordsS_male)
	shortpause_maleS_nbwords = str(shortpause_sums_male["S"]/allwordsS_male)
	corr_maleS_nbwords = str(corr_sums_male["S"]/allwordsS_male)
else:
	euh_maleS_nbwords = "0"
	rep_maleS_nbwords = "0"
	frag_maleS_nbwords = "0"
	shortpause_maleS_nbwords = "0"
	corr_maleS_nbwords = "0"
if allwordsT_male > 0:
	euh_maleT_nbwords = str(euh_sums_male["T"]/allwordsT_male)
	rep_maleT_nbwords = str(rep_sums_male["T"]/allwordsT_male)
	frag_maleT_nbwords = str(frag_sums_male["T"]/allwordsT_male)
	shortpause_maleT_nbwords = str(shortpause_sums_male["T"]/allwordsT_male)
	corr_maleT_nbwords = str(corr_sums_male["T"]/allwordsT_male)
else:
	euh_maleT_nbwords = "0"	
	rep_maleT_nbwords = "0"
	frag_maleT_nbwords = "0"
	shortpause_maleT_nbwords = "0"
	corr_maleT_nbwords = "0"

with open("male_female_disf_nbwords.txt","w",encoding="utf-8") as f:
	f.write("\tMale\n\tNumbers\t\t\t/nb words\n\tS\tT\t\tS\tT\n")
	f.write("EUH\t"+str(euh_sums_male["S"])+"\t"+str(euh_sums_male["T"])+"\t\t"+euh_maleS_nbwords+"\t"+euh_maleT_nbwords+"\n")
	f.write("REP\t"+str(rep_sums_male["S"])+"\t"+str(rep_sums_male["T"])+"\t\t"+rep_maleS_nbwords+"\t"+rep_maleT_nbwords+"\n")
	f.write("CORR\t"+str(corr_sums_male["S"])+"\t"+str(corr_sums_male["T"])+"\t\t"+corr_maleS_nbwords+"\t"+corr_maleT_nbwords+"\n")
	f.write("FRAG\t"+str(frag_sums_male["S"])+"\t"+str(frag_sums_male["T"])+"\t\t"+frag_maleS_nbwords+"\t"+frag_maleT_nbwords+"\n")
	f.write("Short_pause\t"+str(shortpause_sums_male["S"])+"\t"+str(shortpause_sums_male["T"])+"\t\t"+shortpause_maleS_nbwords+"\t"+shortpause_maleT_nbwords+"\n")
	f.write("\tFemale\n\tNumbers\t\t\t/nb words\n\tS\tT\t\tS\tT\n")	
	f.write("EUH\t"+str(euh_sums_female["S"])+"\t"+str(euh_sums_female["T"])+"\t\t"+euh_femaleS_nbwords+"\t"+euh_femaleT_nbwords+"\n")
	f.write("REP\t"+str(rep_sums_female["S"])+"\t"+str(rep_sums_female["T"])+"\t\t"+rep_femaleS_nbwords+"\t"+rep_femaleT_nbwords+"\n")
	f.write("CORR\t"+str(corr_sums_female["S"])+"\t"+str(corr_sums_female["T"])+"\t\t"+corr_femaleS_nbwords+"\t"+corr_femaleT_nbwords+"\n")
	f.write("FRAG\t"+str(frag_sums_female["S"])+"\t"+str(frag_sums_female["T"])+"\t\t"+frag_femaleS_nbwords+"\t"+frag_femaleT_nbwords+"\n")
	f.write("Short_pause\t"+str(shortpause_sums_female["S"])+"\t"+str(shortpause_sums_female["T"])+"\t\t"+shortpause_femaleS_nbwords+"\t"+shortpause_femaleT_nbwords+"\n")
			

with open("disf_STP.txt","w") as resfile1:
	line = "\tS\tT\tP\nCORR\t"+ corr_sums["S"]+" "+corr_sums["T"]+" "+corr_sums["P"]+"\nREP\t"+rep_sums["S"]+" "+rep_sums["T"]+" "+rep_sums["P"]+"\nEUH\t"+euh_sums["S"]+" "+euh_sums["T"]+" "+euh_sums["P"]+"\nshort_pause\t"+shortpause_sums["S"]+" "+shortpause_sums["T"]+" "+shortpause_sums["P"]+"\nFRAG\t"+frag_sums["S"]+" "+frag_sums["T"]+" "+frag_sums["P"]+"\ntotal\t"+total_sums["S"]+" "+total_sums["T"]+" "+total_sums["P"]
	resfile1.write(line)