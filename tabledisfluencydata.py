xmlfilespath = "C:/Python34/myproject/xmlfiles/"
import xml.etree.ElementTree as etree
import os
import re
import argparse
import os
import subprocess

def listDirectory(directory, fileExtList):                                        
    "get list of file info objects for files of particular extensions" 
    fileList = [os.path.normcase(f)
                for f in os.listdir(directory)]             
    Filelist = [os.path.join(directory, f) 
               for f in fileList
                if os.path.splitext(f)[1] in fileExtList] 
    for i in fileList:
        if re.match(r'.*\.D.*', i):
            fileList.remove(i)
    return fileList

class disfsumtableitem:
	def __init__(self,filename, grouptype, gender, Pa_data, Ps_data):
		 self.filename = filename
		 self.grouptype = grouptype
		 self.gender = gender
		 #data for Pa -patient and Ps-psychologist : wordnum, utternum, disfluencies(types and numbers)
		 self.Pa_data = Pa_data
		 self.Ps_data = Ps_data
	  
g1 = disfsumtableitem("jo","T","h",{},{}) 
g1.Pa_data["REP"] = 333
print(g1.Pa_data)

table = []
for file in listDirectory(xmlfilespath, ".xml"):
	tree = etree.ElementTree(file=xmlfilespath+file)
		
	root = tree.getroot()
	if root.get("speakerid") == "control group":
		group = "T"
	else:
		group = "S"
	
	if root.get("speakergender") == "Male":
		gender = "Male"
	if root.get("speakergender") == "Female":
		gender = "Female"
		
	tmpitem = disfsumtableitem(file[:-4].upper(),group,gender,{},{}) 	
	
	#calculate number of words	
	spk1node = root.find("./participants/speaker/[@name='spk1']")
	tmpitem.Ps_data["word_num"] = spk1node.get("word_num")	
	spk2node = root.find("./participants/speaker/[@name='spk2']")
	tmpitem.Pa_data["word_num"] = spk2node.get("word_num")
	#insert utterance numbers
	tmpitem.Ps_data["utterance_num"] = spk1node.get("utterance_num")
	tmpitem.Pa_data["utterance_num"] = spk2node.get("utterance_num")
	#add disfluency data for psychologist
	disfnodePs = spk1node.find("disfluences")# <number> tags for psychologist
	numbersPs = disfnodePs.findall("number")
	for number in numbersPs:
		tmpitem.Ps_data[number.get("type")] = number.text
	
	#add disfluency data for psychologist
	disfnodePa = spk2node.find("disfluences")# <number> tags for psychologist
	numbersPa = disfnodePa.findall("number")
	for number in numbersPa:
		tmpitem.Pa_data[number.get("type")] = number.text
	
	print(tmpitem.filename ,tmpitem.Ps_data)  
	print(tmpitem.Pa_data) 
   
	table.append(tmpitem)
#get list of all present disfluency tags
disf_list = []
disf_list =	[tag for tag in table[0].Pa_data.keys() if tag not in ["utterance_num", "word_num"]]
for item in table[1:]:
	for tag in item.Pa_data.keys():
		if tag not in disf_list and tag not in ["utterance_num", "word_num"]:
			disf_list.append(tag)
	for tag in item.Ps_data.keys():
		if tag not in disf_list and tag not in ["utterance_num", "word_num"]:
			disf_list.append(tag)		
	
print(disf_list)
tag_num = len(disf_list)

fictex = '\documentclass[a4paper, 10pt, landscape]{article}\n \\usepackage[french]{babel}\n \\usepackage[utf8]{inputenc}\n \\usepackage[a4paper]{geometry}\n \\voffset -1.5cm \n\\hoffset -4cm\n'
fictex = fictex+u'\\begin{document}\\begin{table}\\begin{tabular}{|c|c|c|c|c|c|c|' 

for i in range(tag_num*2):
	fictex = fictex + u'c|'
fictex = fictex + u'}\n'
#header
fictex = fictex + u'\\hline\\multicolumn{3}{|c|}{name\\hspace{0.5cm} type\\hspace{0.6cm} gender}&'
#tags names
for tag in disf_list:
	fictex = fictex + u'\\multicolumn{2}{|c|}{'+re.sub(r"_","+",tag)+'} &'
fictex = fictex + u'\\multicolumn{2}{|c|}{nb words} &\\multicolumn{2}{|c|}{nb utterances}\\\\'
#Ps and Pa columns
fictex = fictex + u' \\multicolumn{3}{|c|}{}'
colPsPa = ""
for i in range(tag_num+2):
	colPsPa = colPsPa + u'&Ps&Pa'
fictex = fictex + colPsPa + ' \\\\ \hline '
# adding lines: dialog name, group, gender, disfluency numbers of each type, nb of words, nb of utterances
for line in table:
	tableline = line.filename+u'&'+line.grouptype+u'&'+line.gender
	for tag in disf_list:
		if tag in line.Ps_data.keys():
			tableline = tableline + u'&'+line.Ps_data[tag]
		else:
			tableline = tableline + u'& 0'
			
		if tag in line.Pa_data.keys():
			tableline = tableline + u'&'+line.Pa_data[tag]
		else:
			tableline = tableline + u'& 0'	
	tableline = tableline + u'&'+ line.Ps_data["word_num"]+ u'&'+line.Ps_data["utterance_num"]+ u'&'+line.Pa_data["word_num"]+u'&'+line.Pa_data["utterance_num"] 
	fictex = fictex + tableline + '\\\\'
fictex = fictex + '\hline \\end{tabular}' 
fictex = fictex + "\caption{Table showing disfluency results per each dialog. The info about patient or control group, gender, numbers of disfluencies made of each type, total number of words and utterances is presented. }"	
fictex = fictex + '\\end{table}\n \\end{document}'
	
with open('disfdataperdialog.tex','w') as f:
    f.write(fictex)

cmd = ['pdflatex', '-interaction', 'nonstopmode', 'disfdataperdialog.tex']
proc = subprocess.Popen(cmd)
proc.communicate()

retcode = proc.returncode
if not retcode == 0:
    os.unlink('disfdataperdialog.pdf')
    raise ValueError('Error {} executing command: {}'.format(retcode, ' '.join(cmd))) 

os.unlink('disfdataperdialog.tex')
os.unlink('disfdataperdialog.log')	
	
	


	
