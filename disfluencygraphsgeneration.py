import os
import xml.etree.ElementTree as etree
import numpy as np
import matplotlib.pyplot as plt

xmlfilespath = "xmlfiles"

# get list of file info objects for files of particular extensions
def listDirectory(directory, fileExtList):
    fileList = [os.path.normcase(f)
                for f in os.listdir(directory)]
    Filelist = [os.path.join(directory, f) for f in fileList if os.path.splitext(f)[1] in fileExtList]
    return Filelist


# the method autolabel adds the labels for each bar in dependance of a person who uttered it
# the matching spk1 - p (psychologist), spk2 - a (patient) is chosen by convention and may be changed in the block formimg labels array
def autolabel(rects,labels,n):
    # attach some text labels
    for i in range(n):
        height = rects[i].get_height()
        if height != 0:
            ax.text(rects[i].get_x() + rects[i].get_width()/2., 1.005*height,labels[i],
                    ha='center', va='bottom')


xmlfiles = []
for file in listDirectory(xmlfilespath, ".xml"):
    xmlfiles.append(file)
print(xmlfiles)

for xmlfile in xmlfiles:

    print(" The file "+xmlfile+" is being processed..")
    tree = etree.ElementTree(file=xmlfile)
    root = tree.getroot()

    #get gender
    gender =  root.get("speakergender")
    #get group type
    group_type =  root.get("speakerid")

    utterances = root.find("./topic")
    ind = np.arange(len(utterances))

    #getting list of possible disfluency type
    disfl_types = []
    for utterance in utterances:
        for key in  utterance.keys():
            if "IGN_" in  key:
                if key not in disfl_types:
                    disfl_types.append(key)

    means =[]
    labels = []
    #labels forming
    for utterance in utterances:
        if utterance.attrib["speaker"] == "spk1":
            labels.append("p") # by convention p is psychologist  (spk1)
        else:
            labels.append("a")  # by convention a is patient  (spk2)

    totalmeanset = np.zeros(len(utterances))
    if "TOTAL" not in os.listdir(os.getcwd()):
        os.mkdir("TOTAL")

    for type in disfl_types:
        #directories creation
        if type not in os.listdir(os.getcwd()):
            os.mkdir(type)
        #calculation of the number of disfluencies made
        meanset = np.arange(len(utterances))
        for i in ind:
            if type in utterances[i].keys():
                  meanset[i] = int(utterances[i].attrib[type])
                  totalmeanset[i] += int(utterances[i].attrib[type])
            else:
                meanset[i] = 0
        fig, ax = plt.subplots()
        max_mean = np.amax(meanset)
        ax.set_ylim(0, max_mean+0.5)
        ax.set_ylabel('Disfluency number')
        ax.set_xlabel('Utterances')
        ax.set_title(type+" Group type: "+group_type+" , Gender: "+ gender)
        rects = ax.bar(ind, meanset, 2, color='c')
        autolabel(rects,labels,len(utterances))
        plt.savefig(type+"\/"+xmlfile[-10:-3])

    fig, ax = plt.subplots()
    max_mean = np.amax(totalmeanset)
    ax.set_ylim(0, max_mean+1)
    ax.set_ylabel('Disfluency number')
    ax.set_xlabel('Utterances')
    ax.set_title("TOTAL      Group type: "+group_type+" , Gender: "+ gender)
    rects = ax.bar(ind, totalmeanset, 1, color='c')
    autolabel(rects,labels,len(utterances))
    plt.savefig("TOTAL\/"+xmlfile[-10:-3])
